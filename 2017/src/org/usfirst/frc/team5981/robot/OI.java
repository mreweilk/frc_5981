package org.usfirst.frc.team5981.robot;

import org.usfirst.frc.team5981.robot.RobotMap;

import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

//import org.usfirst.frc.team5981.robot.commands.ExampleCommand;

import org.usfirst.frc.team5981.robot.commands.EmptyHopper;
import org.usfirst.frc.team5981.robot.commands.VacuumBalls;
import org.usfirst.frc.team5981.robot.commands.ClimbRope;
//import org.usfirst.frc.team5981.robot.commands.DriveForwardSomething;
import org.usfirst.frc.team5981.robot.commands.GearBox;
import org.usfirst.frc.team5981.robot.commands.GearBoxClose;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
	private Joystick joystick;
	
	public OI() {
		joystick = new Joystick(RobotMap.controllerPort);
	    Button a_btn = new JoystickButton(joystick, RobotMap.emptyButton);
	    Button b_btn = new JoystickButton(joystick, RobotMap.vacuumButton);
	    Button x_btn = new JoystickButton(joystick, RobotMap.climbButton);
	    //Button y_btn = new JoystickButton(joystick,RobotMap.gearBoxButton);
	    Button rb_btn = new JoystickButton(joystick,RobotMap.gearBoxOpenButton);
	    Button lb_btn = new JoystickButton(joystick,RobotMap.gearBoxCloseButton);
	    
	    a_btn.whileHeld(new EmptyHopper());
	    b_btn.whenPressed(new VacuumBalls());
	    x_btn.whileHeld(new ClimbRope());
	    rb_btn.whenPressed(new GearBox());
	    lb_btn.whenPressed(new GearBoxClose());
	    //rb_btn.whileHeld(new DriveForwardSomething());
		
	}
	
	public Joystick getJoystick() { return joystick; }
}
