package org.usfirst.frc.team5981.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
	public static int controllerPort = 0;
	
	//motor speeds
	
	public static double climberSpeed = 1;
	public static double hopperLoadSpeed = -.5;
	public static double hopperReleaseSpeed = .5;
	
	public static double auto1Distance = 86.5;
	public static double auto2Distance = 56.375;
	public static double autoSpeed = .3;
	public static double turnAngle = 30;
	public static double turnSpeed = .3;
	
	// Xbox 360 Buttons
	public static int emptyButton = 1;
	public static int vacuumButton = 2;
	public static int climbButton = 3;
	public static int gearBoxCloseButton = 6;
	public static int gearBoxOpenButton = 5;
	public static int testCommand = 6;
	
	  // PWM slots
	  public static int frontLeftMotor = 3;
	  public static int rearLeftMotor = 2;
	  public static int frontRightMotor = 4;
	  public static int rearRightMotor = 5;
	  public static int hopperReleaseMotor = 0;
	  public static int hopperLoadMotor = 1;
	  public static int ropeClimbMotor1 = 6;
	  //public static int ropeClimbMotor2 = 6;
	  
	  // PCM slots
	  public static boolean disableCompressor = false;
	  public static int mainCompressor = 0;
	  public static int gearBoxSolenoidForwardChannel = 0;
	  public static int gearBoxSolenoidReverseChannel = 1;
}
