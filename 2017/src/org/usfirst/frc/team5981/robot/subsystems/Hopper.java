package org.usfirst.frc.team5981.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;

import org.usfirst.frc.team5981.robot.RobotMap;

import edu.wpi.first.wpilibj.VictorSP;

public class Hopper extends Subsystem {

	private VictorSP topController;
	private VictorSP bottomController;
	
	public Hopper() {
		topController = new VictorSP(RobotMap.hopperReleaseMotor);
		bottomController = new VictorSP(RobotMap.hopperLoadMotor);
	}
	
	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub

	}
	
	public void releaseHopper() {
		topController.set(RobotMap.hopperReleaseSpeed);
	}
	
	public void stopHopper() {
		topController.set(0);
	}
	
	public void loadHopper() {
		bottomController.set(RobotMap.hopperLoadSpeed);
	}
	
	public void stopLoader() {
		bottomController.set(0);
	}


}
