package org.usfirst.frc.team5981.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.Joystick;

import org.usfirst.frc.team5981.robot.RobotMap;
import org.usfirst.frc.team5981.robot.commands.StickDrive;

/**
 * The drive train subsystem handles all robot driving tasks.
 *
 * @author matt
 */
public class DriveTrain extends Subsystem {

  private VictorSP frontLeft, rearLeft, frontRight, rearRight;
  private RobotDrive drive;

  /**
   * Creates the speed controllers for the drive train's motors.
   */
  public DriveTrain() {
    frontLeft = new VictorSP(RobotMap.frontLeftMotor);
    rearLeft = new VictorSP(RobotMap.rearLeftMotor);
    frontRight = new VictorSP(RobotMap.frontRightMotor);
    rearRight = new VictorSP(RobotMap.rearRightMotor);
    drive = new RobotDrive(frontLeft, rearLeft, frontRight, rearRight);
  }

  /**
   * Set the robot to default to teleop driving.
   */
  @Override
  protected void initDefaultCommand() {
    setDefaultCommand(new StickDrive());
  }

  /**
   * This function enables arcade style driving using the joystick.
   * (Teleop mode)
   * @param joystick the Joystick object
   */
  public void drive(Joystick joystick) {
    drive.arcadeDrive(Math.pow(joystick.getY(),3), -Math.pow(joystick.getX(),3), true);
  }

  /**
   * This function will drive forward at the specified speed.
   * @param speed the speed for the motors (between -1 and 1)
   */
  public void driveForward(double speed, double correction) { drive.drive(-speed, correction); }
  
  public void turnRight(double speed) { drive.drive(0, speed); }
  public void turnLeft(double speed) { drive.drive(0, -speed); }
  
  public void turn(double speed) { drive.arcadeDrive(0,-speed); }

  /**
   * Stops the robot
   */
  public void stop() { drive.drive(0, 0); }
}
