package org.usfirst.frc.team5981.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.ADXRS450_Gyro;

public class Gyro extends Subsystem {

	private static ADXRS450_Gyro gyro;
	
	public Gyro() {
		gyro = new ADXRS450_Gyro();
	}

	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub

	}
	
	public void calibrate() {
		gyro.calibrate();
	}
	public double getAngle() {
		return gyro.getAngle();
	}
	
	public void reset() {
		gyro.reset();
	}
	
	public ADXRS450_Gyro getGyro() {
		return gyro;
	}
}
