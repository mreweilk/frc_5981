package org.usfirst.frc.team5981.robot.subsystems;

import org.usfirst.frc.team5981.robot.RobotMap;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.command.Subsystem;

public class AirStuff extends Subsystem {
	
	  private Compressor compressor;
	  private DoubleSolenoid solenoid;

	public AirStuff() {
	    compressor = new Compressor(RobotMap.mainCompressor);
	    solenoid = new DoubleSolenoid(RobotMap.gearBoxSolenoidForwardChannel,
	    							  RobotMap.gearBoxSolenoidReverseChannel);

	    if (!RobotMap.disableCompressor)
	      compressor.setClosedLoopControl(true);
	}
	
	  /**
	   * Clears faults and turns all solenoids off
	   * (they're probably already off but why not
	   * do it anyways).
	   */
	  @Override
	  protected void initDefaultCommand() {
	    clearFaults();
	    solenoid.set(DoubleSolenoid.Value.kOff);
	  }

	  /**
	   * This will clear sticky faults.
	   */
	  public void clearFaults() {
	    solenoid.clearAllPCMStickyFaults();
	  }
	  
	  public void releaseGearBox() {
		  System.out.println("Releasing");
		  solenoid.set(DoubleSolenoid.Value.kForward);
	  }

	  public void closeGearBox() {
		  System.out.println("Closing");
		  solenoid.set(DoubleSolenoid.Value.kReverse);
	  }
	  
	  public void disableBox() {
		  solenoid.set(DoubleSolenoid.Value.kOff);
	  }
	  

}
