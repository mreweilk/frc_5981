package org.usfirst.frc.team5981.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;

import org.usfirst.frc.team5981.robot.RobotMap;

import edu.wpi.first.wpilibj.Spark;

public class Climber extends Subsystem {

	private Spark motor1, motor2;
	
	public Climber() { 
		motor1 = new Spark(RobotMap.ropeClimbMotor1);
		//motor2 = new Spark(RobotMap.ropeClimbMotor2);
	}
	
	@Override
	protected void initDefaultCommand() {}
	
	public void climb() {
		motor1.set(RobotMap.climberSpeed);
		//motor2.set(RobotMap.climberSpeed);
	}
	
	public void stopClimb() {
		motor1.set(0);
		//motor2.set(0);
	}

}
