package org.usfirst.frc.team5981.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.CounterBase.EncodingType;
import edu.wpi.first.wpilibj.Encoder;

public class Encoders extends Subsystem {

    public static final double kDistancePerRevolution = 18.84;  // guestimate from your code
    public static final double kPulsesPerRevolution = 2048;     // for an AS5145B Magnetic Encoder
    public static final double kDistancePerPulse = kDistancePerRevolution / kPulsesPerRevolution;
    
    private Encoder encoder; 
    
	public Encoders() {
		encoder = new Encoder(1,0,false,EncodingType.k4X);
		encoder.setDistancePerPulse(kDistancePerPulse);
		encoder.setMinRate(2);
		encoder.setSamplesToAverage(7);
	}

	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub

	}


	public double getDistance() {
		return encoder.getDistance();
	}
	
	public double getCount() {
		return encoder.get();
	}
	
	public void reset() {
		encoder.reset();
	}
}
