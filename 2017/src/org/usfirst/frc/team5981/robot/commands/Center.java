package org.usfirst.frc.team5981.robot.commands;

import org.usfirst.frc.team5981.robot.Robot;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDController;
import org.usfirst.frc.team5981.robot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;

public class Center extends Command implements PIDOutput {
	
	static final double kP = 0.03;
	static final double kI = 0.00;
	static final double kD = 0.00;
	static final double kF = 0.00;

	static final double kToleranceDegrees = 2.0f;
	
	private PIDController controller;
	private double turnToAngle;
	
	public Center() {
		requires(Robot.encoders);
		requires(Robot.drivetrain);
		requires(Robot.airstuff);
		requires(Robot.gyro);
		controller = new PIDController(kP,kI,kD,kF,Robot.gyro.getGyro(),this);
		controller.setInputRange(-180.0f, 180.0f);
		controller.setOutputRange(-1.0, 1.0);
		controller.setAbsoluteTolerance(kToleranceDegrees);
		controller.setContinuous(true);
		controller.setSetpoint(0);
	}


	  @Override
	  protected void initialize() {
		  Robot.encoders.reset();
		  Robot.airstuff.releaseGearBox();
		  Robot.gyro.reset();
	  }

	  @Override
	  protected void execute() {
		  if(Robot.encoders.getDistance() < RobotMap.auto1Distance) {
			  Robot.drivetrain.driveForward(RobotMap.autoSpeed,turnToAngle);
		  }
	  }

		@Override
		public void pidWrite(double output) {
			turnToAngle = output;
		}
		
	  /**
	   * Check if ball has launched (currently on timer).
	   */
	  @Override
	  protected boolean isFinished() {
	    return isCanceled();
	  }

	  /**
	   * If ball has been launched.
	   */
	  @Override
	  protected void end() {
	   controller.disable();
	   Robot.drivetrain.stop();
	   Robot.encoders.reset();
	  }

	  /**
	   * If interrupted, call end().
	   */
	  @Override
	  protected void interrupted() {
	    end();
	  }

}
