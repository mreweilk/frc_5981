package org.usfirst.frc.team5981.robot.commands;

import org.usfirst.frc.team5981.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

public class EmptyHopper extends Command {
	
	public EmptyHopper() {
		requires(Robot.hopper);
	}

	  @Override
	  protected void initialize() {
	  }

	  @Override
	  protected void execute() {
		  Robot.hopper.releaseHopper();
	  }

	  /**
	   * Check if ball has launched (currently on timer).
	   */
	  @Override
	  protected boolean isFinished() {
	    return isCanceled();
	  }

	  /**
	   * If ball has been launched.
	   */
	  @Override
	  protected void end() {
	    Robot.hopper.stopHopper();
	  }

	  /**
	   * If interrupted, call end().
	   */
	  @Override
	  protected void interrupted() {
	    end();
	  }


}
