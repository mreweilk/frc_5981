package org.usfirst.frc.team5981.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team5981.robot.Robot;

public class StickDrive extends Command {

	public StickDrive() { requires(Robot.drivetrain); }
	
	  @Override
	  protected void initialize() {}

	  /**
	   * Drives the robot with the joystick.
	   */
	  @Override
	  protected void execute() {
	    Robot.drivetrain.drive(Robot.oi.getJoystick());
	  }

	  /**
	   * Returns true if the command is canceled (it shouldn't ever return true)
	   */
	  @Override
	  protected boolean isFinished() {
	    return isCanceled();
	  }

	  /**
	   * Stops the robot.
	   */
	  @Override
	  protected void end() {
	    Robot.drivetrain.stop();
	  }

	  /**
	   * Stops the robot.
	   */
	  @Override
	  protected void interrupted() {
	    end();
	  }
	}