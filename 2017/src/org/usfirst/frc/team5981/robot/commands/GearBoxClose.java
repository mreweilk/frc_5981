package org.usfirst.frc.team5981.robot.commands;

import org.usfirst.frc.team5981.robot.Robot;
import edu.wpi.first.wpilibj.command.Command;

public class GearBoxClose extends Command {

	public GearBoxClose() {
		requires(Robot.airstuff);
	}

	  @Override
	  protected void initialize() {
		  Robot.airstuff.clearFaults();
		  
	  }

	  @Override
	  protected void execute() {
		  Robot.airstuff.closeGearBox();
	  }
	  
	  /**
	   * Check if ball has launched (currently on timer).
	   */
	  @Override
	  protected boolean isFinished() {
	    return isCanceled();
	  }

	  /**
	   * If ball has been launched.
	   */
	  @Override
	  protected void end() {
		  Robot.airstuff.disableBox();
	  }

	  /**
	   * If interrupted, call end().
	   */
	  @Override
	  protected void interrupted() {
	    end();
	  }


}
