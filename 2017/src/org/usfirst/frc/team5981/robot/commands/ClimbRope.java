package org.usfirst.frc.team5981.robot.commands;

import org.usfirst.frc.team5981.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

public class ClimbRope extends Command {

	public ClimbRope() {
		requires(Robot.climber);
	}


	  @Override
	  protected void initialize() {
	  }

	  @Override
	  protected void execute() {
		  Robot.climber.climb();
	  }
	  
	  /**
	   * Check if ball has launched (currently on timer).
	   */
	  @Override
	  protected boolean isFinished() {
	    return isCanceled();
	  }

	  /**
	   * If ball has been launched.
	   */
	  @Override
	  protected void end() {
		  Robot.climber.stopClimb();
	  }

	  /**
	   * If interrupted, call end().
	   */
	  @Override
	  protected void interrupted() {
	    end();
	  }

}
