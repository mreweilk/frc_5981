package org.usfirst.frc.team5981.robot.commands;

import org.usfirst.frc.team5981.robot.Robot;
import org.usfirst.frc.team5981.robot.RobotMap;

import edu.wpi.first.wpilibj.command.Command;

public class Right extends Command {

	private boolean firstTurn;
	private double distance;
	
	public Right() {
		requires(Robot.encoders);
		requires(Robot.drivetrain);
	}


	  @Override
	  protected void initialize() {
		  Robot.encoders.reset();
		  firstTurn = false;
	  }

	  @Override
	  protected void execute() {
		  /**if(Robot.encoders.getDistance() < RobotMap.auto1Distance) {
			  Robot.drivetrain.driveForward(RobotMap.autoSpeed);
			  //System.out.println("Encoder distance: "+Robot.encoders.getDistance());
		  } else {
			  if(Robot.gyro.getAngle() < RobotMap.turnAngle) {
				  Robot.drivetrain.turn(RobotMap.turnSpeed);
			  } else {
				  firstTurn = true;
				  distance = Robot.encoders.getDistance();
			  }
		  }
		  
		  if(firstTurn == true) {
			  if((Robot.encoders.getDistance()-distance) < RobotMap.auto2Distance) {
				  Robot.drivetrain.driveForward(RobotMap.autoSpeed);
			  }
		  }**/
	  }

	  /**
	   * Check if ball has launched (currently on timer).
	   */
	  @Override
	  protected boolean isFinished() {
	    return isCanceled();
	  }

	  /**
	   * If ball has been launched.
	   */
	  @Override
	  protected void end() {
	   Robot.drivetrain.stop();
	   Robot.encoders.reset();
	  }

	  /**
	   * If interrupted, call end().
	   */
	  @Override
	  protected void interrupted() {
	    end();
	  }

}
