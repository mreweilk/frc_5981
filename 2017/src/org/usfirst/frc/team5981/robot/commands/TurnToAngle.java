package org.usfirst.frc.team5981.robot.commands;

import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDController;
import org.usfirst.frc.team5981.robot.Robot;


import edu.wpi.first.wpilibj.command.Command;

public class TurnToAngle extends Command implements PIDOutput {

	//http://www.pdocs.kauailabs.com/navx-mxp/examples/rotate-to-angle-2/
	static final double kP = 0.03;
	static final double kI = 0.00;
	static final double kD = 0.00;
	static final double kF = 0.00;

	static final double kToleranceDegrees = 2.0f;
	
	private PIDController controller;
	private double turnToAngle;
	
	public TurnToAngle(double angle) {
		requires(Robot.drivetrain);
		requires(Robot.gyro);
		controller = new PIDController(kP,kI,kD,kF,Robot.gyro.getGyro(),this);
		controller.setInputRange(-180.0f, 180.0f);
		controller.setOutputRange(-1.0, 1.0);
		controller.setAbsoluteTolerance(kToleranceDegrees);
		controller.setContinuous(true);
		controller.setSetpoint(angle);
	}
	
	@Override
	protected void initialize() {
		Robot.gyro.reset();
		controller.enable();
	}
	
	@Override
	protected void execute() {
		Robot.drivetrain.turn(turnToAngle);
	}
	  
	@Override
	public void pidWrite(double output) {
		turnToAngle = output;
	}

	@Override
	protected void interrupted() {
	  end();
	}
	
	@Override
	protected void end() {
	   controller.disable();
	   Robot.gyro.reset();
	   Robot.drivetrain.stop();
	}
	 
	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return controller.onTarget();
	}

}
