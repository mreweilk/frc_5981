package org.usfirst.frc.team5981.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;

import org.usfirst.frc.team5981.robot.RobotConfig;

/**
 * The pneumatics subsystem is responsible for
 * controlling the compressor and the two double
 * solenoids.
 *
 * @author matt
 *
 */
public class Pneumatics extends Subsystem {

  private Compressor compressor;
  private DoubleSolenoid solenoid1;
  private Solenoid solenoid2;

  /**
   * This subsystem controls the pneumatic components
   * of the robot.
   */
  public Pneumatics() {
    compressor = new Compressor(RobotConfig.mainCompressor);
    solenoid1 =
        new DoubleSolenoid(RobotConfig.launcherArmSolenoidForwardChannel,
                           RobotConfig.launcherArmSolenoidReverseChannel);
    solenoid2 = new Solenoid(RobotConfig.firingPinSolenoidForwardChannel);

    if (!RobotConfig.disableCompressor)
      compressor.setClosedLoopControl(true);
  }

  /**
   * Clears faults and turns all solenoids off
   * (they're probably already off but why not
   * do it anyways).
   */
  @Override
  protected void initDefaultCommand() {
    clearFaults();
    solenoid1.set(DoubleSolenoid.Value.kOff);
  }

  /**
   * This will clear sticky faults.
   */
  public void clearFaults() {
    solenoid1.clearAllPCMStickyFaults();
    solenoid2.clearAllPCMStickyFaults();
  }
  /**
   * Raises the launcher up
   */
  public void raiseLauncher() { solenoid1.set(DoubleSolenoid.Value.kForward); }

  /**
   * Lowers the launcher down
   */
  public void lowerLauncher() { solenoid1.set(DoubleSolenoid.Value.kReverse); }

  /**
   * Freezes the launcher
   */
  public void disableLauncher() { solenoid1.set(DoubleSolenoid.Value.kOff); }

  /**
   * Launcher the ball.
   */
  public void triggerFiringPin() {
    solenoid2.set(true);
    Timer.delay(0.2);
    solenoid2.set(false);
  }

  /**
   * Reset the firing pin.
   */
  public void resetFiringPin() {
    // solenoid2.set(false);
  }
}
