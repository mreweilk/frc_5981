package org.usfirst.frc.team5981.robot.subsystems;

import org.usfirst.frc.team5981.robot.Robot;
import org.usfirst.frc.team5981.robot.RobotConfig;

import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.command.Subsystem;
import org.usfirst.frc.team5981.robot.subsystems.Pneumatics;

/**
 * The Launcher subsystem controls the ball launcher.
 * The launcher assembly must be at 0 degrees when
 * starting the robot in order for any sort of accuracy when
 * raising and lowering the launcher assembly.
 * @author matt
 *
 */
public class Launcher extends Subsystem {

  private VictorSP leftWheel, rightWheel;
  private Pneumatics pneumatics;

  private boolean launcherPosition = false;

  public Launcher(Pneumatics pneumatics) {
    this.pneumatics = pneumatics;
    leftWheel = new VictorSP(RobotConfig.leftLauncherMotor);
    rightWheel = new VictorSP(RobotConfig.rightLauncherMotor);
    setCollector(true);
  }

  @Override
  protected void initDefaultCommand() {}

  /**
   * This function sets the direction the two motors spin,
   * if the passed parameter is true, the motors will spin
   * to collect a ball, if false, they will spin to release
   * a ball.
   *
   * @param collect determines which direction the launcher assembly motors spin
   */
  public void setCollector(boolean collect) {
    if (collect == true) {
      leftWheel.setInverted(true);
      rightWheel.setInverted(false);
    } else {
      leftWheel.setInverted(false);
      rightWheel.setInverted(true);
    }
  }

  /**
   * Raises the launcher assembly into the firing position.
   */
  public void moveLauncherUp() {
    pneumatics.raiseLauncher();
    launcherPosition = true;
  }

  /**
   * Lowers the launcher assembly out of the firing position.
   */
  public void moveLauncherDown() {
    pneumatics.lowerLauncher();
    launcherPosition = false;
  }

  /**
   * Disables the launcher assembly pneumatics valves.
   */
  public void disableLauncher() {
    pneumatics.disableLauncher();
    launcherPosition = false;
  }

  /**
   * Clears any potential faults of the pneumatics subsystem.
   */
  public void clearFaults() { Robot.pneumatics.clearFaults(); }

  /**
   * This function sets the motors to the passed speed parameter.
   *
   * @param speed the speed the launcher assembly motors spin (between -1 and 1)
   */
  public void setMotors(double speed) {
    leftWheel.set(speed);
    rightWheel.set(speed);
  }

  /**
   * Performs preperation for launching the ball.
   */
  public void launchPrep() {
    setCollector(true);
    setMotors(RobotConfig.defaultCollectSpeed);
  }

  /**
   * Returns to default ball collection state.
   */
  public void postLaunch() {
    setCollector(false);
    setMotors(0);
  }

  /**
   * Enables ball collection.
   */
  public void collectBall() {
    setCollector(false);
    setMotors(RobotConfig.defaultCollectSpeed);
  }

  /**
   * Disables ball collection.
   */
  public void stopCollection() {
    setCollector(false);
    setMotors(0);
  }

  /**
   * Triggers the firing pin to launch the ball.
   */
  public void triggerFiringPin() { Robot.pneumatics.triggerFiringPin(); }

  /**
   * Resets the firing pin to the unfired position.
   */
  public void resetFiringPin() { Robot.pneumatics.resetFiringPin(); }

  /**
   * Returns current status of the launcher assembly.
   *
   * @return launcherPosition
   */
  public boolean isRaised() { return launcherPosition; }
}
