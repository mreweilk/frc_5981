package org.usfirst.frc.team5981.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.Spark;

import org.usfirst.frc.team5981.robot.RobotConfig;

/**
 * The zipper subsystem is responsible for the zipper arm.
 *
 * @author matt
 *
 */
public class Zipper extends Subsystem {

  private Spark zipperMotor;

  /**
   * Create a speed controller for the zipper arm motor.
   */
  public Zipper() { zipperMotor = new Spark(RobotConfig.zipperDriveMotor); }

  /**
   * Command run on initialization.
   */
  @Override
  protected void initDefaultCommand() {}

  /**
   * Raise the zipper.
   */
  public void raiseZipper() { zipperMotor.set(RobotConfig.zipperRaiseSpeed); }

  /**
   * Lower the zipper.
   */
  public void lowerZipper() { zipperMotor.set(RobotConfig.zipperLowerSpeed); }

  /**
   * Stop lowering or raising the zipper.
   */
  public void stopZipper() { zipperMotor.set(0); }
}
