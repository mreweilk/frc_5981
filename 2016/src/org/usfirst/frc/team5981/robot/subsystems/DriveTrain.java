package org.usfirst.frc.team5981.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.Joystick;

import org.usfirst.frc.team5981.robot.RobotConfig;
import org.usfirst.frc.team5981.robot.commands.StickDrive;

/**
 * The drive train subsystem handles all robot driving tasks.
 *
 * @author matt
 */
public class DriveTrain extends Subsystem {

  private VictorSP frontLeft, rearLeft, frontRight, rearRight;
  private RobotDrive drive;

  /**
   * Creates the speed controllers for the drive train's motors.
   */
  public DriveTrain() {
    frontLeft = new VictorSP(RobotConfig.frontLeftMotor);
    rearLeft = new VictorSP(RobotConfig.rearLeftMotor);
    frontRight = new VictorSP(RobotConfig.frontRightMotor);
    rearRight = new VictorSP(RobotConfig.rearRightMotor);
    drive = new RobotDrive(frontLeft, rearLeft, frontRight, rearRight);
  }

  /**
   * Set the robot to default to teleop driving.
   */
  @Override
  protected void initDefaultCommand() {
    setDefaultCommand(new StickDrive());
  }

  /**
   * This function enables arcade style driving using the joystick.
   * (Teleop mode)
   * @param joystick the Joystick object
   */
  public void drive(Joystick joystick) {
    drive.setSensitivity(0.001);
    drive.arcadeDrive(joystick.getY(), -joystick.getX(), true);
  }

  /**
   * This function will drive forward at the specified speed.
   * @param speed the speed for the motors (between -1 and 1)
   */
  public void driveForward(double speed) { drive.drive(speed, 0); }

  /**
   * Stops the robot
   */
  public void stop() { drive.drive(0, 0); }
}
