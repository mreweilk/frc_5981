
package org.usfirst.frc.team5981.robot;

import edu.wpi.first.wpilibj.IterativeRobot;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.networktables.NetworkTable;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
//import edu.wpi.first.wpilibj.CameraServer;

// Command imports
import org.usfirst.frc.team5981.robot.commands.LowBarAuto;
import org.usfirst.frc.team5981.robot.commands.RoughTerrainAuto;
import org.usfirst.frc.team5981.robot.commands.DoNothingAuto;

// Subsystem imoprts
import org.usfirst.frc.team5981.robot.subsystems.DriveTrain;
import org.usfirst.frc.team5981.robot.subsystems.Launcher;
import org.usfirst.frc.team5981.robot.subsystems.Pneumatics;
import org.usfirst.frc.team5981.robot.subsystems.Zipper;

/**
 * This is the code for the Thunderbotz Team 5981 2016 FRC entry
 *
 * @author matt
 *
 */
public class Robot extends IterativeRobot {

  public static double GROUND_ANGLE = 0;
  public static OI oi;
  public static DriveTrain drivetrain;
  public static Launcher launcher;
  public static Pneumatics pneumatics;
  public static Zipper zipper;

  Command autonomousCommand;
  //CameraServer camera;
  SendableChooser autoChooser;

  /**
   * Create the operator interface and the various subsystem instances and
   * the autonomous defense selector.
   */
  public void robotInit() {
    autoChooser = new SendableChooser();
    autoChooser.initTable(NetworkTable.getTable("Defense Chooser"));
    drivetrain = new DriveTrain();
    pneumatics = new Pneumatics();
    launcher = new Launcher(pneumatics);
    zipper = new Zipper();
    //camera = CameraServer.getInstance();
    autonomousCommand = new LowBarAuto();

    //camera.setQuality(75);
    //camera.startAutomaticCapture("cam0");

    oi = new OI();

    autoChooser.addDefault("Low bar", new LowBarAuto());
    autoChooser.addObject("Rough Terrain", new RoughTerrainAuto());
    autoChooser.addObject("Do nothing", new DoNothingAuto());
    SmartDashboard.putData("Autonomous Mode Chooser", autoChooser);
  }

  /**
   * This function is called once each time the robot enters Disabled mode.
   * You can use it to reset any subsystem information you want to clear when
   * the robot is disabled.
   */
  public void disabledInit() {}

  public void disabledPeriodic() { Scheduler.getInstance().run(); }

  /**
   * Start the autonomous command.
   */
  public void autonomousInit() {
    autonomousCommand = (Command)autoChooser.getSelected();
    autonomousCommand.start();
  }

  /**
   * This function is called periodically during autonomous
   */
  public void autonomousPeriodic() { Scheduler.getInstance().run(); }

  public void teleopInit() {
    // This makes sure that the autonomous stops running when
    // teleop starts running. If you want the autonomous to
    // continue until interrupted by another command, remove
    // this line or comment it out.
    if (autonomousCommand != null)
      autonomousCommand.cancel();
  }

  /**
   * This function is called periodically during operator control
   */
  public void teleopPeriodic() { Scheduler.getInstance().run(); }

  /**
   * This function is called periodically during test mode
   */
  public void testPeriodic() { LiveWindow.run(); }
}
