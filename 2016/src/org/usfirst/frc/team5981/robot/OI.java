package org.usfirst.frc.team5981.robot;

import org.usfirst.frc.team5981.robot.RobotConfig;

// Control imports
import edu.wpi.first.wpilibj.buttons.Button;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import edu.wpi.first.wpilibj.Joystick;

// Command imports
import org.usfirst.frc.team5981.robot.commands.FireBall;
import org.usfirst.frc.team5981.robot.commands.LowerLauncher;
import org.usfirst.frc.team5981.robot.commands.RaiseLauncher;
import org.usfirst.frc.team5981.robot.commands.CollectBall;
import org.usfirst.frc.team5981.robot.commands.SpinLauncherMotors;
// import org.usfirst.frc.team5981.robot.commands.RaiseZipper;
// import org.usfirst.frc.team5981.robot.commands.LowerZipper;

/**
 * This class sets the various commands to be triggered by the
 * various buttons which trigger them.
 *
 * @author matt
 */
public class OI {

  private Joystick joystick;

  /**
   * This creates the joystick object and the various buttons which will
   * be used by the robot in teleop driving mode.
   */
  public OI() {
    joystick = new Joystick(RobotConfig.joystickPort);

    /* Xbox 360 controller */
    Button a_btn = new JoystickButton(joystick, RobotConfig.triggerButton);
    // Button b_btn = new JoystickButton(joystick,RobotConfig.collectButton);
    Button x_btn = new JoystickButton(joystick, RobotConfig.lowerButton);
    Button y_btn = new JoystickButton(joystick, RobotConfig.raiseButton);
    Button l1_btn =
        new JoystickButton(joystick, RobotConfig.spinLauncherMotors);
    Button l2_btn = new JoystickButton(joystick, RobotConfig.collectButton);
    // Button back_btn = new
    // JoystickButton(joystick,RobotConfig.raiseZipperButton);
    // Button start_btn = new
    // JoystickButton(joystick,RobotConfig.lowerZipperButton);

    /* Commands */
    l1_btn.whileHeld(new SpinLauncherMotors());
    l2_btn.whileHeld(new CollectBall());
    a_btn.whileHeld(new FireBall());
    x_btn.whenPressed(new LowerLauncher());
    y_btn.whenPressed(new RaiseLauncher());
    // back_btn.whileHeld(new RaiseZipper());
    // start_btn.whileHeld(new LowerZipper());
  }

  /**
   * Returns the joystick.
   *
   * @return joystick
   */
  public Joystick getJoystick() { return joystick; }
}
