package org.usfirst.frc.team5981.robot;

/**
 * Configuraton of the robot.
 *
 * @author matt
 *
 */
public class RobotConfig {
  // Autonomous speed/timing
  public static double autoLowBarTimeout = 3.0;
  public static double autoLowBarSpeed = -0.4;
  public static double autoRoughtTerrainTimeout = 3.0;
  public static double autoRoughTerrainSpeed = -0.8;

  // Joystick
  public static int joystickPort = 0;

  // Xbox 360 Buttons
  public static int triggerButton = 1;
  public static int collectButton = 6; // 5 for L2 button
  public static int lowerButton = 3;
  public static int raiseButton = 4;
  public static int spinLauncherMotors = 5;
  public static int raiseZipperButton = 7;
  public static int lowerZipperButton = 8;

  // PWM slots
  public static int frontLeftMotor = 1;
  public static int rearLeftMotor = 2;
  public static int frontRightMotor = 4;
  public static int rearRightMotor = 5;
  public static int leftLauncherMotor = 0;
  public static int rightLauncherMotor = 3;
  public static int zipperDriveMotor = 6;

  // PCM slots
  public static int mainCompressor = 0;
  public static int launcherArmSolenoidForwardChannel = 0;
  public static int launcherArmSolenoidReverseChannel = 1;
  public static int firingPinSolenoidForwardChannel = 2;
  public static int firingPinSolenoidReverseChannel = 3;

  // MISC
  public static boolean disableCompressor = false;
  public static boolean safetyEnabled = false;
  public static double defaultLaunchSpeed = 1;
  public static double defaultCollectSpeed = 0.5;
  public static double zipperRaiseSpeed = 1;
  public static double zipperLowerSpeed = -1;
}
