package org.usfirst.frc.team5981.robot.commands;

import org.usfirst.frc.team5981.robot.Robot;

import edu.wpi.first.wpilibj.command.Command;

/**
 * If there is no defense we can handle, do nothing!
 *
 * @author matt
 *
 */
public class DoNothingAuto extends Command {

  public DoNothingAuto() { requires(Robot.drivetrain); }

  /**
   * Does absolutely nothing!
   */
  @Override
  protected void initialize() {}

  /**
   * Does absolutely nothing!
   */
  @Override
  protected void execute() {}

  /**
   * Return true once autonomous mode ends.
   */
  @Override
  protected boolean isFinished() {
    return isCanceled();
  }

  /**
   * Set joystick driving once autonomous mode is canceled.
   */
  @Override
  protected void end() {
    Robot.drivetrain.drive(Robot.oi.getJoystick());
  }

  /**
   * Set up joystick on interruption.
   */
  @Override
  protected void interrupted() {
    end();
  }
}
