package org.usfirst.frc.team5981.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team5981.robot.Robot;

/**
 * This is a simple command that spins the motors on the launcher
 * assembly in order to launch the ball.
 *
 * @author matt
 */
public class SpinLauncherMotors extends Command {

  /**
   * This command requires the launcher subsystem.
   */
  public SpinLauncherMotors() { requires(Robot.launcher); }

  /**
   * Start the motors
   */
  @Override
  protected void initialize() {
    Robot.launcher.launchPrep();
  }

  @Override
  protected void execute() {}

  /**
   * Returns true when the button is released.
   */
  @Override
  protected boolean isFinished() {
    return isCanceled();
  }

  /**
   * Stops the motors.
   */
  @Override
  protected void end() {
    Robot.launcher.postLaunch();
  }

  /**
   * Stops the motors.
   */
  @Override
  protected void interrupted() {
    end();
  }
}
