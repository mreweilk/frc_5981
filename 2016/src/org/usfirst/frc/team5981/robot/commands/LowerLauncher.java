package org.usfirst.frc.team5981.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team5981.robot.Robot;

/**
 * This is a simple command that lowers the launcher.
 *
 * @author matt
 */
public class LowerLauncher extends Command {

  /**
   * Requires the pneumatics subsystem.
   */
  public LowerLauncher() { requires(Robot.launcher); }

  /**
   * Clear faults when command is first run.
   */
  @Override
  protected void initialize() {
    Robot.launcher.clearFaults();
  }

  /**
   * Lowers the launcher.
   */
  @Override
  protected void execute() {
    Robot.launcher.moveLauncherDown();
  }

  /**
   * Checks if button is released.
   */
  @Override
  protected boolean isFinished() {
    return isCanceled();
  }

  /**
   * When button is released, freeze the launcher.
   */
  @Override
  protected void end() {
    Robot.launcher.disableLauncher();
  }

  /**
   * When command interrupted, freeze the launcher.
   */
  @Override
  protected void interrupted() {
    end();
  }
}
