package org.usfirst.frc.team5981.robot.commands;

import org.usfirst.frc.team5981.robot.Robot;
import org.usfirst.frc.team5981.robot.RobotConfig;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

/**
 * This class will control the Autonomous operation of the robot.
 * In this instance, the robot is setup for the rough terrain defense.
 *
 * @author matt
 *
 */
public class RoughTerrainAuto extends Command {

  private Timer timer;

  /**
   * Secure use of the drive train and set up a new timer.
   */
  public RoughTerrainAuto() {
    requires(Robot.drivetrain);
    timer = new Timer();
  }

  /**
   * Secure use of the drive train and set up a new timer.
   */
  @Override
  protected void initialize() {
    timer.start();
    Robot.launcher.moveLauncherUp();
  }

  /**
   * Drive forward (at increased speed in order to clear properly) and stop.
   */
  @Override
  protected void execute() {
    double currTime = timer.get();
    if (currTime < RobotConfig.autoRoughtTerrainTimeout)
      Robot.drivetrain.driveForward(RobotConfig.autoRoughTerrainSpeed);
    else
      Robot.drivetrain.stop();
  }

  /**
   * Return true once autonomous mode ends.
   */
  @Override
  protected boolean isFinished() {
    return isCanceled();
  }

  /**
   * Set joystick driving once autonomous mode is canceled.
   */
  @Override
  protected void end() {
    Robot.drivetrain.drive(Robot.oi.getJoystick());
  }

  /**
   * Set up joystick on interruption.
   */
  @Override
  protected void interrupted() {
    end();
  }
}
