package org.usfirst.frc.team5981.robot.commands;

import org.usfirst.frc.team5981.robot.Robot;
import edu.wpi.first.wpilibj.command.Command;

/**
 * Command to lower the zipper arm.
 *
 * @author matt
 *
 */
public class LowerZipper extends Command {

  /**
   * Secures use of the zipper.
   */
  public LowerZipper() { requires(Robot.zipper); }

  /**
   * Sets up command.
   */
  @Override
  protected void initialize() {}

  /**
   * Lowers the zipper.
   */
  @Override
  protected void execute() {
    Robot.zipper.lowerZipper();
  }

  /**
   * Returns true when the button is released.
   */
  @Override
  protected boolean isFinished() {
    return isCanceled();
  }

  /**
   * Stops the zipper.
   */
  @Override
  protected void end() {
    Robot.zipper.stopZipper();
  }

  /**
   * Stops the zipper when interrupted.
   */
  @Override
  protected void interrupted() {
    end();
  }
}
