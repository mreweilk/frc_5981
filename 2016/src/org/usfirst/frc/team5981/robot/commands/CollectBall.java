package org.usfirst.frc.team5981.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team5981.robot.Robot;

/**
 * Simple command to enable ball collection.
 *
 * @author matt
 *
 */
public class CollectBall extends Command {

  /**
   * Requires the launcher subsystem.
   */
  public CollectBall() { requires(Robot.launcher); }

  /**
   * Starts the ball collection routines.
   */
  @Override
  protected void initialize() {
    Robot.launcher.collectBall();
  }

  @Override
  protected void execute() {}

  /**
   * Returns true if the button is released.
   */
  @Override
  protected boolean isFinished() {
    return isCanceled();
  }

  /**
   * Stops the ball collection routines.
   */
  @Override
  protected void end() {
    Robot.launcher.stopCollection();
  }

  /**
   * Stops the ball collection routines.
   */
  @Override
  protected void interrupted() {
    end();
  }
}
