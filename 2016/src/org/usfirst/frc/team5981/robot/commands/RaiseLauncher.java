package org.usfirst.frc.team5981.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team5981.robot.Robot;

/**
 * This is a simple command that raises the launcher.
 *
 * @author matt
 */
public class RaiseLauncher extends Command {

  /**
   * Requires the pneumatics subsystem.
   */
  public RaiseLauncher() { requires(Robot.launcher); }

  /**
   * Clear faults when command is first run.
   */
  @Override
  protected void initialize() {
    Robot.launcher.clearFaults();
  }

  /**
   * Raises the launcher.
   */
  @Override
  protected void execute() {
    Robot.launcher.moveLauncherUp();
  }

  /**
   * Checks if button is released.
   */
  @Override
  protected boolean isFinished() {
    return isCanceled();
  }

  /**
   * When button is released, freeze the launcher.
   */
  @Override
  protected void end() {
    Robot.launcher.disableLauncher();
  }

  /**
   * When command interrupted, freeze the launcher.
   */
  @Override
  protected void interrupted() {
    end();
  }
}
