package org.usfirst.frc.team5981.robot.commands;

import org.usfirst.frc.team5981.robot.Robot;
import edu.wpi.first.wpilibj.command.Command;

/**
 * Command to raise the zipper arm.
 *
 * @author matt
 *
 */
public class RaiseZipper extends Command {

  /**
   * Secures use of the zipper.
   */
  public RaiseZipper() { requires(Robot.zipper); }

  /**
   * Sets up command.
   */
  @Override
  protected void initialize() {}

  /**
   * Raises the zipper.
   */
  @Override
  protected void execute() {
    Robot.zipper.raiseZipper();
  }

  /**
   * Returns true when the button is released.
   */
  @Override
  protected boolean isFinished() {
    return isCanceled();
  }

  /**
   * Stops the zipper.
   */
  @Override
  protected void end() {
    Robot.zipper.stopZipper();
  }

  /**
   * Stops the zipper when interrupted.
   */
  @Override
  protected void interrupted() {
    end();
  }
}
