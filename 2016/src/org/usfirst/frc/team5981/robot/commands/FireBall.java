package org.usfirst.frc.team5981.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc.team5981.robot.Robot;
import org.usfirst.frc.team5981.robot.RobotConfig;

/**
 * This command handles the firing of the ball and is
 * triggered by the 'A' button on the xbox controller
 * (or the firing trigger if using the joystick).
 *
 * @author matt
 *
 */
public class FireBall extends Command {

  /**
   * Requires access to the launcher and pneumatics
   * subsystems and sets a timeout of half a second.
   */
  public FireBall() { requires(Robot.pneumatics); }

  /**
   * Sets the launcher assembly motors into firing
   * mode and triggers the firing pin.
   */
  @Override
  protected void initialize() {
    if (!RobotConfig.safetyEnabled) {
      Robot.launcher.triggerFiringPin();
    }
  }

  @Override
  protected void execute() {}

  /**
   * Check if ball has launched (currently on timer).
   */
  @Override
  protected boolean isFinished() {
    return isCanceled();
  }

  /**
   * If ball has been launched.
   */
  @Override
  protected void end() {
    if (!RobotConfig.safetyEnabled) {
      Robot.launcher.resetFiringPin();
    }
  }

  /**
   * If interrupted, call end().
   */
  @Override
  protected void interrupted() {
    end();
  }
}
