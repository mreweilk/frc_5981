package org.usfirst.frc.team5981.robot.commands;

import org.usfirst.frc.team5981.robot.Robot;
import org.usfirst.frc.team5981.robot.RobotConfig;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;

/**
 * This class will control the Autonomous operation of the robot.
 * In this instance, the robot is setup for the low bar defense.
 *
 * @author matt
 *
 */
public class LowBarAuto extends Command {

  private Timer timer;

  /**
   * Secure use of the drive train and set up a new timer.
   */
  public LowBarAuto() {
    requires(Robot.drivetrain);
    timer = new Timer();
  }

  /**
   * Start the timer.
   */
  @Override
  protected void initialize() {
    timer.start();
  }

  /**
   * Drive forward for a certain time, then reverse back to
   * the starting position.
   */
  @Override
  protected void execute() {
    double currTime = timer.get();
    if (currTime < RobotConfig.autoLowBarTimeout) {
      Robot.drivetrain.driveForward(RobotConfig.autoLowBarSpeed);
    } else if (currTime >= RobotConfig.autoLowBarTimeout &&
               currTime < RobotConfig.autoLowBarTimeout * 2) {
      Robot.drivetrain.driveForward(-RobotConfig.autoLowBarSpeed);
    } else {
      Robot.drivetrain.stop();
    }
  }

  /**
   * Return true once autonomous mode ends.
   */
  @Override
  protected boolean isFinished() {
    return isCanceled();
  }

  /**
   * Set joystick driving once autonomous mode is canceled.
   */
  @Override
  protected void end() {
    Robot.drivetrain.drive(Robot.oi.getJoystick());
  }

  /**
   * Set up joystick on interruption.
   */
  @Override
  protected void interrupted() {
    end();
  }
}
